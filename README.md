# zsh-theme

1. `sudo apt install zsh zsh-autosuggestions zsh-syntax-highlighting zsh-common zsh-antigen -y`
2.  `git clone https://github.com/azizyahyaoui/zsh-theme.git`
3.  `cp zsh-theme/zshrcfile.txt ~/.zshrc`
4.  `sudo chsh -s /bin/zsh`
